CXX:=g++
CXXFLAGS:=-Wall -Wextra -pedantic -std=c++14
RELEASEF:=-O3
DEBUGF:=-ggdb -O0 -DDEBUG_MODE
LDFLAGS:=-lsfml-graphics -lsfml-window -lsfml-system

CPPFLAGS:=-I./src

SRC:=$(wildcard src/*.cpp) \
	 $(wildcard src/ui/*.cpp) \
	 $(wildcard src/components/*.cpp)

OBJ:=$(patsubst %.cpp,%.o, $(SRC))
NO_OF_FILES:=$(words $(OBJ))
COUNT:=0

PRG:=raspiInterface

.PHONY:all clean

all:debug

release:CXXFLAGS += $(RELEASEF)
release:$(PRG)
	@echo "Release build"

debug:CXXFLAGS += $(DEBUGF) 
debug:$(PRG)
	@echo "Debug build"

clang_complete:
	@echo $(CPPFLAGS) | tr " " "\n" > .clang_complete
	@echo ".clang_complete generated"

$(PRG):$(OBJ)
	@$(CXX) $(CXXFLAGS) $(CPPFLAGS) $^ -o $@ $(LDFLAGS)
	@$(eval COUNT=$(shell echo $$(($(COUNT)+1))))
	@echo "Compiled $@"

%.o:%.cpp
	@$(CXX) $(CXXFLAGS) $(CPPFLAGS) -c $< -o $@ $(LDFLAGS)
	@$(eval COUNT=$(shell echo $$(($(COUNT)+1))))
	@echo "[$(COUNT) / $(NO_OF_FILES)] Compiled $@"

clean:
	rm -rf $(OBJ)
	rm -rf $(PRG)
