#include "button.h" 

bvd::Button::Button(const sf::Font& font)
{
	text.setFont(font);
	text.setString("Untitled");

	updateShape();
}
	

void bvd::Button::updateShape()
{
	text.setCharacterSize((height/100) * 60);

	sf::FloatRect rect = text.getLocalBounds();
	text.setOrigin(rect.left + (rect.width / 2.0f), rect.top + (rect.height / 2.0f));

	sf::Vector2f pos = getPosition();
	text.setPosition(sf::Vector2f(pos.x + (width/2.0f), pos.y + (height/2.0f)));

	if(drawable)
	{
		drawable->setWidth(this->width);
		drawable->setHeight(this->height);
	}
}

sf::Vector2f bvd::Button::getPosition()
{
	if(drawable)
	{
		return drawable->getPosition();
	}

	return sf::Vector2f(0, 0);
}

void bvd::Button::setPosition(float x, float y)
{
	drawable->setPosition(x, y);
	updateShape();
}

void bvd::Button::setSize(float width, float height)
{
	this->width = width;
	this->height = height;
	updateShape();
}

void bvd::Button::setDecoration(bvd::drawable_ptr shape)
{
	if (shape)
	{
		drawable = std::move(shape);
		this->width = drawable->getSize().x;
		this->height = drawable->getSize().y;
	}
}

void bvd::Button::setColor(sf::Color color)
{
	text.setFillColor(color);
	if (drawable)
	{
		drawable->setColor(color);
	}
}

float bvd::Button::setText(const std::string& text)
{
	this->text.setString(text);
	float tempWidth = this->text.getLocalBounds().width;
	if (drawable)
	{
		tempWidth += 2 * drawable->getThickness();
	}
	return tempWidth;
}

bool bvd::Button::checkAreaEntered(sf::Vector2f point, sf::Vector2f position, sf::Vector2f size)
{
	bool xPos = (point.x >= position.x) && (point.x <= (position.x + size.x));
	bool yPos = (point.y >= position.y) && (point.y <= (position.y + size.y));

	if(xPos && yPos)
	{
		return true;
	}
	return false;
}

bool bvd::Button::handleClick(const sf::Event& event)
{
	if (event.type == sf::Event::MouseButtonPressed)
	{
		sf::Vector2f mousePos = { static_cast<float>(event.mouseButton.x), static_cast<float>(event.mouseButton.y) };
		if(checkAreaEntered(mousePos, drawable->getPosition(), drawable->getSize()))
		{
			setColor(clickedColor);
			return true;
		}
	}
	else if (event.type == sf::Event::MouseButtonReleased)
	{
		sf::Vector2f mousePos = { static_cast<float>(event.mouseButton.x), static_cast<float>(event.mouseButton.y) };
		if(checkAreaEntered(mousePos, drawable->getPosition(), drawable->getSize()))
		{
			setColor(activeColor);
		}
	}

	return false;
}

void bvd::Button::draw(sf::RenderTarget& target, sf::RenderStates states) const
{
	if(drawable)
	{
		target.draw(*(drawable.get()), states);
	}
	target.draw(text, states);
}
