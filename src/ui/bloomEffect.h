#ifndef BVD_BLOOMEFFECT_H
#define BVD_BLOOMEFFECT_H

#include <SFML/Graphics/RenderTexture.hpp>
#include <SFML/Graphics/Shader.hpp>
#include <SFML/Graphics/VertexArray.hpp>

namespace bvd
{
	class BloomEffect
	{
	public:
		BloomEffect();
		void apply(const sf::RenderTexture& input, sf::RenderTarget& output);
		void prepareTextures(sf::Vector2u size);
		void add(const sf::RenderTexture& source, const sf::RenderTexture& bloom, sf::RenderTarget& output);
		void blurMultipass(std::array<sf::RenderTexture, 2>& renderTextures);
		void blur(const sf::RenderTexture& input, sf::RenderTexture& output, sf::Vector2f offsetFactor);
		void downsample(const sf::RenderTexture& input, sf::RenderTexture& output);
		void filterBright(const sf::RenderTexture& input, sf::RenderTexture& output);
		void applyShader(const sf::Shader& shader, sf::RenderTarget& output);
		
	private:
		sf::Shader gaussianBlurPass;
		sf::Shader brightnessPass;
		sf::Shader addPass;
		sf::Shader downSamplePass;
		sf::RenderTexture mBrightnessTexture;

		std::array<sf::RenderTexture, 2> mFirstPassTextures;
		std::array<sf::RenderTexture, 2> mSecondPassTextures;
	};
}
#endif
