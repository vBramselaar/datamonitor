#include "decoration.h"


void bvd::Decoration::draw(sf::RenderTarget& target, sf::RenderStates states) const
{
	states.transform *= getTransform();
	states.texture = nullptr;

	// you may also override states.shader or states.blendMode if you want

	target.draw(vertices, states);
}

void bvd::Decoration::setWidth(float width)
{
	size = sf::Vector2f(width, size.y);
	update();
}

void bvd::Decoration::setHeight(float height)
{
	size = sf::Vector2f(size.x, height);
	update();
}

void bvd::Decoration::setThickness(float thickness)
{
	this->thickness = thickness;
	update();
}

void bvd::Decoration::setCornerLength(float cornerLength)
{
	this->cornerLength = cornerLength;
	update();
}

void bvd::Decoration::setColor(sf::Color color)
{
	for(size_t i = 0; i < vertices.getVertexCount(); i++)
	{
		vertices[i].color = color;
	}
}
