#ifndef BVD_DECORATION_H
#define BVD_DECORATION_H

#include <SFML/Graphics.hpp>

namespace bvd
{
	class Decoration : public sf::Drawable, public sf::Transformable
	{
	public:
		void setWidth(float width);
		void setHeight(float height);
		void setThickness(float thickness);
		void setCornerLength(float cornerLength);
		void setColor(sf::Color color);
		float getThickness() { return thickness; }

		sf::Vector2f getSize() { return size; }

	protected:
		sf::VertexArray vertices;

		sf::Vector2f size;
		float cornerLength;
		float thickness;
		
	private:
		void draw(sf::RenderTarget& target, sf::RenderStates states) const;
		virtual void update() = 0;
	};
}

#endif
