#include "bloomEffect.h"

bvd::BloomEffect::BloomEffect()
{
	if (!gaussianBlurPass.loadFromFile("./src/resources/shaders/Fullpas.vert", "./src/resources/shaders/GuassianBlur.frag"))
	{
		throw std::runtime_error("BloomEffect: GaussianBlurPass Failed to load");
	}
	
	if (!brightnessPass.loadFromFile("./src/resources/shaders/Fullpas.vert", "./src/resources/shaders/Brightness.frag"))
	{
		throw std::runtime_error("BloomEffect: BrightnessPass Failed to load");
	}
	
	if (!addPass.loadFromFile("./src/resources/shaders/Fullpas.vert", "./src/resources/shaders/Add.frag"))
	{
		throw std::runtime_error("BloomEffect: AddPass Failed to load");
	}

	if (!downSamplePass.loadFromFile("./src/resources/shaders/Fullpas.vert", "./src/resources/shaders/DownSample.frag"))
	{
		throw std::runtime_error("BloomEffect: AddPass Failed to load");
	}
}

void bvd::BloomEffect::apply(const sf::RenderTexture& input, sf::RenderTarget& output)
{
	prepareTextures(input.getSize());

	filterBright(input, mBrightnessTexture);

	downsample(mBrightnessTexture, mFirstPassTextures[0]);
	blurMultipass(mFirstPassTextures);

	downsample(mFirstPassTextures[0], mSecondPassTextures[0]);
	blurMultipass(mSecondPassTextures);

	add(mFirstPassTextures[0], mSecondPassTextures[0], mFirstPassTextures[1]);
	mFirstPassTextures[1].display();
	add(input, mFirstPassTextures[1], output);
}

void bvd::BloomEffect::prepareTextures(sf::Vector2u size)
{
	if (mBrightnessTexture.getSize() != size)
	{
		mBrightnessTexture.create(size.x, size.y);
		mBrightnessTexture.setSmooth(true);

		mFirstPassTextures[0].create(size.x / 2, size.y / 2);
		mFirstPassTextures[0].setSmooth(true);
		mFirstPassTextures[1].create(size.x / 2, size.y / 2);
		mFirstPassTextures[1].setSmooth(true);

		mSecondPassTextures[0].create(size.x / 4, size.y / 4);
		mSecondPassTextures[0].setSmooth(true);
		mSecondPassTextures[1].create(size.x / 4, size.y / 4);
		mSecondPassTextures[1].setSmooth(true);
	}
}

void bvd::BloomEffect::blurMultipass(std::array<sf::RenderTexture, 2>& renderTextures)
{
	sf::Vector2u textureSize = renderTextures[0].getSize();

	for (std::size_t count = 0; count < 2; ++count)
	{
		blur(renderTextures[0], renderTextures[1], sf::Vector2f(0.f, 1.f / textureSize.y));
		blur(renderTextures[1], renderTextures[0], sf::Vector2f(1.f / textureSize.x, 0.f));
	}
}

void bvd::BloomEffect::add(const sf::RenderTexture& source, const sf::RenderTexture& bloom, sf::RenderTarget& output)
{
	addPass.setUniform("source", source.getTexture());
	addPass.setUniform("bloom", bloom.getTexture());
	applyShader(addPass, output);
}

void bvd::BloomEffect::blur(const sf::RenderTexture& input, sf::RenderTexture& output, sf::Vector2f offsetFactor)
{
	gaussianBlurPass.setUniform("source", input.getTexture());
	gaussianBlurPass.setUniform("offsetFactor", offsetFactor);
	applyShader(gaussianBlurPass, output);
	output.display();
}

void bvd::BloomEffect::downsample(const sf::RenderTexture& input, sf::RenderTexture& output)
{
	downSamplePass.setUniform("source", input.getTexture());
	downSamplePass.setUniform("sourceSize", sf::Vector2f(input.getSize()));
	applyShader(downSamplePass, output);
	output.display();
}

void bvd::BloomEffect::filterBright(const sf::RenderTexture& input, sf::RenderTexture& output)
{
	brightnessPass.setUniform("source", input.getTexture());
	applyShader(brightnessPass, output);
	output.display();
}

void  bvd::BloomEffect::applyShader(const sf::Shader& shader, sf::RenderTarget& output)
{
	sf::Vector2f outputSize = static_cast<sf::Vector2f>(output.getSize());

	sf::VertexArray vertices(sf::TrianglesStrip, 4);
	vertices[0] = sf::Vertex(sf::Vector2f(0, 0),            sf::Vector2f(0, 1));
	vertices[1] = sf::Vertex(sf::Vector2f(outputSize.x, 0), sf::Vector2f(1, 1));
	vertices[2] = sf::Vertex(sf::Vector2f(0, outputSize.y), sf::Vector2f(0, 0));
	vertices[3] = sf::Vertex(sf::Vector2f(outputSize),      sf::Vector2f(1, 0));

	sf::RenderStates states;
	states.shader 	 = &shader;
	states.blendMode = sf::BlendNone;

	output.draw(vertices, states);
}
