#include "unfilledOctagon.h"

bvd::UnfilledOctagonShape::UnfilledOctagonShape(sf::Vector2f pos, sf::Vector2f size,
												float thickness, float cornerLength)
{
	vertices = sf::VertexArray(sf::Quads, 32);

	this->setPosition(pos.x, pos.y);
	this->thickness = thickness;
	this->cornerLength = cornerLength;
	this->size = size;
	update();
}

void bvd::UnfilledOctagonShape::update()
{
	float cornerDis = cornerLength * sin(0.25 * M_PI)/sin(0.5 * M_PI);
	float extraSpace = tan(22.5 * M_PI / 180.0) * thickness;
	float shortDis = thickness + cornerDis;
	float longDis = shortDis - extraSpace;

	sf::Vector2f pos = sf::Vector2f(0,0);
	
	vertices[0].position = sf::Vector2f(pos.x + longDis, pos.y);
	vertices[1].position = sf::Vector2f(pos.x + (size.x - longDis), pos.y);
	vertices[2].position = sf::Vector2f(pos.x + (size.x - shortDis), pos.y + thickness);
	vertices[3].position = sf::Vector2f(pos.x + shortDis, pos.y + thickness);

	vertices[4].position = vertices[1].position;
	vertices[5].position = sf::Vector2f(pos.x + size.x, pos.y + longDis);
	vertices[6].position = sf::Vector2f(pos.x + (size.x - thickness), pos.y + shortDis);
	vertices[7].position = vertices[2].position;

	vertices[8].position = vertices[6].position;
	vertices[9].position = vertices[5].position;
	vertices[10].position = sf::Vector2f(pos.x + size.x, pos.y + (size.y - longDis));
	vertices[11].position = sf::Vector2f(pos.x + (size.x - thickness), pos.y + (size.y - shortDis));

	vertices[12].position = vertices[11].position;
	vertices[13].position = vertices[10].position;
	vertices[14].position = sf::Vector2f(pos.x + (size.x - longDis), pos.y + size.y);
	vertices[15].position = sf::Vector2f(pos.x + (size.x - shortDis), pos.y + (size.y - thickness));

	vertices[16].position = sf::Vector2f(pos.x + shortDis, pos.y + (size.y - thickness));
	vertices[17].position = vertices[15].position;
	vertices[18].position = vertices[14].position;
	vertices[19].position = sf::Vector2f(pos.x + longDis, pos.y + size.y);

	vertices[20].position = sf::Vector2f(pos.x, pos.y + (size.y - longDis));
	vertices[21].position = sf::Vector2f(pos.x + thickness, pos.y + (size.y - shortDis));
	vertices[22].position = vertices[16].position;
	vertices[23].position = vertices[19].position;

	vertices[24].position = sf::Vector2f(pos.x, pos.y + longDis);
	vertices[25].position = sf::Vector2f(pos.x + thickness, pos.y + shortDis);
	vertices[26].position = vertices[21].position;
	vertices[27].position = vertices[20].position;

	vertices[28].position = vertices[0].position;
	vertices[29].position = vertices[3].position;
	vertices[30].position = vertices[25].position;
	vertices[31].position = vertices[24].position;
}
