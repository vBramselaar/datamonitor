#ifndef BVD_BUTTON_H
#define BVD_BUTTON_H

#include <SFML/Graphics.hpp>
#include <cmath>
#include <memory>
#include <utility>
#include "ui/unfilledOctagon.h"
#include "ui/decoration.h"
#include "utils/logger.h"

namespace bvd
{
	using drawable_ptr=std::unique_ptr<bvd::Decoration>;
	
	class Button : public sf::Drawable
	{
	public:
		Button(const sf::Font& font);
		bool handleClick(const sf::Event& event);
		
		void setPosition(float x, float y);
		void setSize(float width, float height);
		void setDecoration(bvd::drawable_ptr shape);
		float setText(const std::string& text);
		void setActiveColor(sf::Color newColor) { activeColor = newColor; }
		void setClickedColor(sf::Color newColor) { clickedColor = newColor; } 
		
		sf::Vector2f getSize() { return sf::Vector2f(width, height); }
		sf::Vector2f getPosition();

	private:
		bvd::drawable_ptr drawable; 
		sf::Text text;
		sf::Texture m_texture;

		sf::Color activeColor = sf::Color::White;
		sf::Color clickedColor = sf::Color(100, 100, 150);
		
		float width = 150.f;
		float height = 50.f;

		void setColor(sf::Color color);
		bool checkAreaEntered(sf::Vector2f point, sf::Vector2f position, sf::Vector2f size);
		virtual void draw(sf::RenderTarget& target, sf::RenderStates states) const;
		void updateShape();
	};
}
	
#endif
