#ifndef BVD_TWOCORNORRECTANGLE_H
#define BVD_TWOCORNORRECTANGLE_H

#include <SFML/Graphics.hpp>
#include <cmath>
#include "decoration.h"

namespace bvd
{
	class UnfilledOctagonShape : public bvd::Decoration
	{
	public:
		UnfilledOctagonShape(sf::Vector2f pos = sf::Vector2f(0, 0), sf::Vector2f size = sf::Vector2f(350, 100),
							 float thickness = 10, float cornerLength = 10);

	private:
		void update() override;
	};
}
	
#endif
