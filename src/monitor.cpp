#include "monitor.h" 

bvd::Monitor::Monitor()
{
	std::vector<sf::VideoMode> modes = sf::VideoMode::getFullscreenModes();
	if(modes.empty())
	{
		throw std::runtime_error("No suitable video-mode found");
	}

	window = new sf::RenderWindow(sf::VideoMode(modes[0].width, modes[0].height, modes[0].bitsPerPixel),
								  "Data monitor",
								  sf::Style::Fullscreen);

	window->setFramerateLimit(60);
	LOG("Monitor turned on");
	LOG("W: " << modes[0].width << " H: " << modes[0].height << " BPP: " << modes[0].bitsPerPixel);
}

bvd::Monitor::~Monitor()
{
	delete window;
	LOG("Monitor turned off");
}

void bvd::Monitor::addComponent(bvd::component_ptr newComponent)
{
	newComponent->setPos({window->getSize().x * 0.5f, window->getSize().y * 0.5f});
	components.push_back(newComponent);
}

void bvd::Monitor::handleInput()
{
	sf::Event event;
	while (window->pollEvent(event))
	{
		if(event.type == sf::Event::Closed)
		{
			window->close();
		}

		if (event.type == sf::Event::Resized)
		{
			sf::FloatRect visibleArea(0,0 ,event.size.width, event.size.height);
			window->setView(sf::View(visibleArea));
		}

		for (std::size_t i = 0; i < components.size(); i++)
		{
			if (components[i]->handleInput(event))
			{
				selectedComponent = i;
				LOG("Selected view: " << selectedComponent);
			}
		}

	}
}

void bvd::Monitor::update()
{
	if (!components.empty())
	{
		components.at(selectedComponent)->update();
	}
}

void bvd::Monitor::draw()
{
	window->clear();
	/*
	  sf::RenderTexture texture;
	  texture.create(1920, 1080);
	  texture.clear();
	  sf::RenderTexture blurredTexture;
	  blurredTexture.create(1920, 1080);
	  blurredTexture.clear();
	  texture.draw(button);
	  texture.display();
	  effect.apply(texture, blurredTexture);
	  sf::Sprite sprite(blurredTexture.getTexture());
	*/

	if (!components.empty())
	{
		window->draw(*(components.at(selectedComponent)));
	}
	
	window->display();
}
