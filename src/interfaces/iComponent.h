#ifndef BVD_ICOMPONENT_H
#define BVD_ICOMPONENT_H

#include <SFML/Graphics.hpp>
#include "utils/types.h"

namespace bvd
{
	class iComponent : public sf::Drawable
	{
	public:
		virtual ~iComponent() {}

		virtual void setSize(sf::Vector2f size) = 0;
		virtual void setPos(sf::Vector2f pos) = 0;
		virtual bool handleInput(const sf::Event& event) = 0;
		virtual void update() = 0;
	};
}

#endif
