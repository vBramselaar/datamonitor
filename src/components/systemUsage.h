#ifndef BVD_SYSTEMUSAGE_H
#define BVD_SYSTEMUSAGE_H

#include <SFML/Graphics.hpp>
#include <string>
#include <fstream>
#include <iostream>
#include <chrono>
#include <sstream>
#include "utils/logger.h"
#include "interfaces/iComponent.h"
#include "ui/button.h"

namespace bvd
{
	class SystemUsage : public iComponent
	{
	public:
		SystemUsage(const sf::Font& font);
		~SystemUsage();

		sf::Vector2f getSize() { return size; }
		sf::Vector2f getPos() { return pos; }
		void setSize(sf::Vector2f size) override;
		void setPos(sf::Vector2f pos) override;
		void update() override;
		bool handleInput(const sf::Event& event) override;
		
	private:
		sf::Text text;
		std::string buffer = "--- --- ";
		int currentTextSize = 40;
		std::chrono::steady_clock::time_point timeChecker = std::chrono::steady_clock::now();

		bvd::Button selectButton;
		
		sf::Vector2f pos = {0, 0};
		sf::Vector2f size = {500, 500};

		void updateBuffer();
		void updateText();
		void draw(sf::RenderTarget& target, sf::RenderStates states) const override;
	};
}

#endif
