#include "systemUsage.h" 


bvd::SystemUsage::SystemUsage(const sf::Font& font) : selectButton(font)
{
	text.setFont(font);
	text.setCharacterSize(currentTextSize);

	selectButton.setDecoration(std::move(std::make_unique<UnfilledOctagonShape>()));
	selectButton.setPosition(50, 50);
	
	float width = selectButton.setText("System Usage");
	selectButton.setSize(width + 30, selectButton.getSize().y);

	updateText();

	LOG("SystemUsage started");
}

bvd::SystemUsage::~SystemUsage()
{
	LOG("SystemUsage ended");
}

void bvd::SystemUsage::setSize(sf::Vector2f size)
{
	this->size = size;
}

void bvd::SystemUsage::setPos(sf::Vector2f pos)
{
	this->pos = pos;
	updateText();
}


void bvd::SystemUsage::update()
{
    std::chrono::duration<double> elapsed_seconds = std::chrono::steady_clock::now() - timeChecker;

	if(elapsed_seconds.count() >= 2)
	{
		updateBuffer();
		updateText();
		timeChecker = std::chrono::steady_clock::now();
	}
}

bool bvd::SystemUsage::handleInput(const sf::Event &event)
{
	bool check = selectButton.handleClick(event);

	return check;
}

void bvd::SystemUsage::updateText()
{
	std::string formattedBuffer = "Total Memory: " + buffer.substr(0, buffer.find(' ')) + " KB\n";
	formattedBuffer += "Memory Free: " + buffer.substr(buffer.find(' ')) + "KB";
	
	text.setString(formattedBuffer);

	sf::FloatRect rect = text.getLocalBounds();
	text.setOrigin(rect.left + (rect.width / 2.0f), rect.top + (rect.height / 2.0f));

	text.setPosition(pos.x, pos.y);
}

void bvd::SystemUsage::updateBuffer()
{
	std::ifstream memFile("/proc/meminfo");
	buffer.clear();

	int amountOflines = 0;
	std::string line;
	while(std::getline(memFile, line) && amountOflines < 2)
	{
		std::size_t sep = line.find(' ');
		line = line.substr(sep, line.find_last_of(' ') - sep);
		line.erase(remove_if(line.begin(), line.end(), isspace), line.end());
		
		buffer += line + " ";
		amountOflines++;
	}
}

void bvd::SystemUsage::draw(sf::RenderTarget& target, sf::RenderStates states) const
{
	target.draw(selectButton, states);
	target.draw(text, states);
}
