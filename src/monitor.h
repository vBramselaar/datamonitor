#ifndef BVD_MONITOR_H
#define BVD_MONITOR_H

#include <SFML/Graphics.hpp>
#include <stdexcept>
#include <vector>
#include <memory>
#include "utils/logger.h"
#include "ui/bloomEffect.h"
#include "interfaces/iComponent.h"

namespace bvd
{
	using component_ptr = std::shared_ptr<bvd::iComponent>;
	
	class Monitor
	{
	public:
		Monitor();
		~Monitor();

		void addComponent(bvd::component_ptr);
		void handleInput();
		void update();
		void draw();
		bool isRunning() { return window->isOpen(); }
		
	private:
		sf::RenderWindow* window;
		bvd::BloomEffect effect;

		std::vector<bvd::component_ptr> components;
		std::size_t selectedComponent = 0;
	};
}
	
#endif
