#ifndef BVD_LOGGER_H
#define BVD_LOGGER_H

#include <cstring>
#include <iostream>

#ifdef _MSC_VER
#define __FILENAME__ strrchr("\\" __FILE__, '\\') + 1
#else
#define __FILENAME__ strrchr("/" __FILE__, '/') + 1
#endif

#define LOG(ARG) std::cout << "LOG: " << __FILENAME__ << " " << __LINE__ << ": " << ARG << std::endl;
#define ERROR(ARG) std::cerr << "\033[0;31mERROR\033[0m: " << __FILENAME__ << " " << __LINE__ << ": " << ARG << std::endl;

#ifdef DEBUG_MODE
#define DEBUG(ARG) std::cout << "DEBUG: " << __FILENAME__ << " " << __LINE__ << ": " << ARG << std::endl;
#else 
#define DEBUG(ARG) do{}while(0)
#endif

#endif
