#include <SFML/Graphics/Font.hpp>
#include "monitor.h"
#include "components/systemUsage.h"

int main()
{
	bvd::Monitor monitor;

	sf::Font font;
	if (!font.loadFromFile("./src/resources/fonts/LiberationSans.ttf"))
	{
		ERROR("Could not load font");
		return -1;
	}
	
	std::shared_ptr<bvd::SystemUsage> usage = std::make_shared<bvd::SystemUsage>(font);

	monitor.addComponent(usage);
	
	while(monitor.isRunning())
	{
		monitor.handleInput();
		monitor.update();
		monitor.draw();
	}
	
    return 0;
}
